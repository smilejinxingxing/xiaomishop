function startMove(domobj, json, fn) {
    clearInterval(domobj.timer);
    domobj.timer = setInterval(function () {
      var flag = true; //假设所有的都达到目标值
      for (var attr in json) {
        if (attr == "opacity") {
          var iCur = parseInt(getStyle(domobj, "opacity") * 100);
        } else {
          var iCur = parseInt(getStyle(domobj, attr));
        }
  
        var iTarget = json[attr];
        var iSpeed = (iTarget - iCur) / 8;
        iSpeed = iSpeed > 0 ? Math.ceil(iSpeed) : Math.floor(iSpeed);
  
        if (attr == "opacity") {
          domobj.style.opacity = (iCur + iSpeed) / 100;
          domobj.style.filter = "alpha(opacity=" + (iCur + iSpeed) + ")";
        } else {
          domobj.style[attr] = iCur + iSpeed + "px";
        }
  
  
        if (iCur != iTarget) {
          flag = false;
        }
      }
      if (flag) {
        clearInterval(domobj.timer);
        if (fn) {
          fn();
        }
  
      }
    }, 20);
  }
  
  function getStyle(domobj, attr) {
    if (window.getComputedStyle) {
      return getComputedStyle(domobj, null)[attr];
    }
    return domobj.currentStyle[attr];
  }